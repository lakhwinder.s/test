<?php

namespace App\Controllers;

use \CodeIgniter\Controller;

use App\Models\UsersModel;
use App\Models\UsersModel2;
use App\Models\FetchModel;


class Main extends BaseController
{
	public function index()
	{
		return view('register');
	}
	public function savethis()
	{
		$validation = $this->validate([
			'name'=>'required',
			'password'=>'required',
			'address'=>'required'
		]);
		if(!$validation){
			return view('register',['validation'=>$this->validator]);
	
	}
	else{
		$name= $this->request->getPost('name');
		$password= $this->request->getPost('password');
		$address= $this->request->getPost('address');
		
		

		$values=[
			'name'=>$name,
			'password'=> $password,
			'address'=>$address,
		];
		$usersmodel = new \App\Models\UsersModel();
		$query= $usersmodel->insert($values);
		if(!$query){
			return redirect()->back()->with('fail','something wrong');
		}else{
			return redirect()->to('register')->with('sucess','sucees stored information');
		}


	}
	

	}
	public function new(){
		$data =[
			'meta_title'=>'new post',
			'title' =>'create a new post',
		];
		if ($this->request->getMethod()=='post'){
			$model =new UsersModel();
			$model->save($_POST);
		}

		return view('new_post',$data);
	}




	public function new3(){
		$name= $this->request->getPost('name');
		$password= $this->request->getPost('password');
		$address= $this->request->getPost('address');
		
		

		$data=[
			'name'=>$name,
			'password'=> $password,
			'address'=>$address,
		];
		
		$UsersModel = new \App\Models\UsersModel();

		$query= $UsersModel->insert($data);
		
		if(!$query){
			return redirect()->back()->with('fail','something wrong');
		}else{
			return redirect()->to('register')->with('sucess','sucees stored information');
		}

	}
	public function create(){
		$UsersModel = new UsersModel();
		$UsersModel->save([
		'name'=>$this->request->getPost('name'),
		'password'=> $this->request->getPost('password'),
		'address'=> $this->request->getPost('address')

		]);

	}
	public function getdata(){
	$UsersModel2 = new UsersModel2();
	$data['subject']=$UsersModel2->getData();
	print_r($data['subject']);

	}
	public function fetch(){
	
	$FetchModel = new FetchModel();
	
	$data['login']=$FetchModel->orderBy('id','DESC'
	)->paginate(10);
	$data['pagination_link']= $FetchModel->pager;

	return view('Fetch_data',$data);
}

}
